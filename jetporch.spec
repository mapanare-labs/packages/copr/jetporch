%global giturl  https://github.com/jetporch/jetporch

Name:		jetporch
Version:	0.0.1
Release:	1%{?dist}
Summary:	Next generation IT orchestration

License:	GPL 3.0
URL:		https://www.jetporch.com/
Source0:     https://github.com/jetporch/jetporch/archive/refs/tags/v%{version}.tar.gz
BuildRequires: git
BuildRequires: rust
BuildRequires: cargo
BuildRequires:  make
BuildRequires:  openssl
BuildRequires:  openssl-devel

%description
Jet is a general-purpose, community-driven IT automation platform 
for configuration, deployment, orchestration, patching, and arbitrary 
task execution workflows. 

%prep
%setup -q -n jetporch-%{commit} -c
mv jetporch-%{commit} jetporch

%build
cd %{name}
make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/bin
install -p -m 755 target/release/jetp %{buildroot}/usr/bin

%files
#%license COPYING
%doc README.md
/usr/bin/jetp

%changelog
* Fri Sep 29 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Initial release
